<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index()
    {
        // mengambil data dari table tbl_mahasiswa
        $mahasiswa = DB::table('mahasiswa')->get();

        // mengirim data mahasiswa ke view index
        return view('index',['mahasiswa' => $mahasiswa]);
    }
    // method untuk menampilkan view form tambah mahasiswa
public function tambah()
    {
        // memanggil view tambah
        return view('tambah');
    }
    // method untuk insert data ke table tbl_Mahasiswa
public function store(Request $request)
{
    // insert data ke table tbl_Mahasiswa
    DB::table('Mahasiswa')->insert([
        'nama_mahasiswa' => $request->nama_mahasiswa,
        'nim_mahasiswa' => $request->nim_mahasiswa,
        'kelas_mahasiswa' => $request->kelas_mahasiswa,
        'prodi_mahasiswa' => $request->prodi_mahasiswa,
        'fakultas_mahasiswa' => $request->fakultas_mahasiswa]
    );

    // alihkan halaman ke halaman Mahasiswa
    return redirect('/mahasiswa');
}
// method untuk edit data mahasiswa
public function edit($id)
{ 
    // mengambil data siswa berdasarkan id yang dipilih
    $mahasiswa = DB::table('mahasiswa')->where('id',$id)->get(); 
    
    // passing data siswa yang didapat ke view edit.blade.php 
    return view('edit',['mahasiswa' => $mahasiswa]);
}
// update data mahasiswa
public function update(Request $request)
{
    // update data mahasiswa
    DB::table('mahasiswa')->where('id',$request->id)->update([

        'nama_mahasiswa' => $request->nama_mahasiswa,
        'nim_mahasiswa' => $request->nim_mahasiswa,
        'kelas_mahasiswa' => $request->kelas_mahasiswa,
        'prodi_mahasiswa' => $request->prodi_mahasiswa,
        'fakultas_mahasiswa' => $request->fakultas_mahasiswa
    ]);

    // alihkan halaman ke halaman siswa
    return redirect('/mahasiswa');
}
// method untuk hapus data mahasiswa
public function hapus($id)
{
    // menghapus data mahasiswa berdasarkan id yang dipilih
    DB::table('mahasiswa')->where('id',$id)->delete();
    
    // alihkan halaman ke halaman mahasiswa
    return redirect('/mahasiswa');
}
}