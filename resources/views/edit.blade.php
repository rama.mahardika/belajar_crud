<!DOCTYPE html>
<html>
    <head>
        <title>Tutorial CRUD Pada Laravel</title>
    </head>

    <body>
      
        <h3>Edit Mahasiswa</h3>
        <a href="/mahasiswa"> Kembali</a>
        <br/>
        <br/>
        
        @foreach($mahasiswa as $s)
        <form action="/mahasiswa/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $s->id }}"> 
            <br/>
            Nama mahasiswa <input type="text" required="required" name="nama_mahasiswa" value="{{ $s->nama_mahasiswa}}"> 
            <br/>
            Nim mahasiswa <input type="text" required="required" name="nim_mahasiswa" value="{{ $s->nim_mahasiswa }}"> 
            <br/>
            Kelas mahasiswa <input type="text" required="required" name="kelas_mahasiswa" value="{{ $s->kelas_mahasiswa }}">
            <br/>
            prodi mahasiswa <input type="text" required="required" name="prodi_mahasiswa" value="{{ $s->prodi_mahasiswa }}">
            <br/>
            Fakultas mahasiswa <input type="text" required="required" name="fakultas_mahasiswa" value="{{ $s->prodi_mahasiswa }}"> <br/>
            <input type="submit" value="Simpan Data">

        </form>
        
        @endforeach
    </body>
</html>