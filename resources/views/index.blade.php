<!DOCTYPE html>
<html>
    <head>
        <title>Tutorial Membuat CRUD Pada Laravel 8</title>
    </head>

    <body>
        
        <h3>Data mahasiswa</h3>

        <a href="/mahasiswa/tambah"> + Tambah mahasiswa Baru +</a>

        <br/>
        <br/>

        <table border="1">
            <tr>
                <th>nama</th>
                <th>nim</th>
                <th>kelas</th>
                <th>prodi</th>
                <th>fakultas</th>
            </tr>

            @foreach($mahasiswa as $p)
            <tr>
                <td>{{ $p->nama_mahasiswa }}</td>
                <td>{{ $p->nim_mahasiswa }}</td>
                <td>{{ $p->kelas_mahasiswa }}</td>
                <td>{{ $p->prodi_mahasiswa }}</td>
                <td>{{ $p->fakultas_mahasiswa }}</td>
                <td>
                    <a href="/mahasiswa/edit/{{ $p->id }}">Edit</a>
                    |
                    <a href="/mahasiswa/hapus/{{ $p->id }}">Hapus</a>
                </td>
            </tr>
            @endforeach
        </table>
    </body>
</html>