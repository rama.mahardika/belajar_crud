<!DOCTYPE html> 
<html> 
    <head> 
        <title>Tutorial CRUD Pada Laravel</title> 
    </head> 
    
    <body> 
        

        <h3>Data Siswa</h3> 
        <a href="/mahasiswa"> Kembali</a> 
        <br/> 
        <br/> 
        
        <form action="/mahasiswa/store" method="post"> 
            {{ csrf_field() }} 
            <br /> 
            nama   <input type="text" name="nama_mahasiswa" required="required"> 
            <br /> 
            nim   <input type="number" name="nim_mahasiswa" required="required"> 
            <br /> 
            kelas   <input type="text" name="kelas_mahasiswa" required="required"> 
            <br /> 
            prodi      <input type="text" name="prodi_mahasiswa" required="required"> 
            <br /> 
            fakultas      <input type="text" name="fakultas_mahasiswa" required="required"> 
            <br /> 
            <input type="submit" value="Simpan Data"> 
        </form> 
    </body> 
</html>